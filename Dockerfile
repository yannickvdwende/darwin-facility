FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 60660
EXPOSE 44362

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["../darwin-facility/darwin-facility.csproj", "../darwin-facility/"]
RUN dotnet restore "../darwin-facility/darwin-facility.csproj"
COPY . .
WORKDIR "/src/../darwin-facility"
RUN dotnet build "darwin-facility.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "darwin-facility.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "darwin-facility.dll"]