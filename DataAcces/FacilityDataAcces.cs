﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using darwin_outlook_model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace darwin_facility.DataAcces
{
    public class FacilityDataAcces
    {
            MongoClient _client;
            IMongoDatabase _db;

            public FacilityDataAcces()
            {
                _client = new MongoClient("mongodb://0.tcp.ngrok.io:10442");
                _db = _client.GetDatabase("darwin");
            }

            public IEnumerable<FacilityModel> GetMeetingModels()
            {
                try
                {
                    return _db.GetCollection<FacilityModel>("FacilityModels").Find(_ => true).ToList();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }


            public async Task<FacilityModel> GetMeetingModel(ObjectId id)
            {
                var x = await _db.GetCollection<FacilityModel>("FacilityModels").Find(p => ObjectId.Parse(p.Id) == id).SingleAsync();

                return x;
            }

            public async Task<FacilityModel> Create(FacilityModel p)
            {
                try
                {
                    await _db.GetCollection<FacilityModel>("FacilityModels").InsertOneAsync(p);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                return p;
            }

            public async void Update(ObjectId id, FacilityModel p)
            {
                p.Id = id.ToString();
                await _db.GetCollection<FacilityModel>("FacilityModels").ReplaceOneAsync(item => item.Id == p.Id, p, new UpdateOptions { IsUpsert = true });

            }
            public async void Remove(ObjectId id)
            {
                await _db.GetCollection<FacilityModel>("FacilityModels").DeleteOneAsync(item => ObjectId.Parse(item.Id) == id);
            }
        
    }
}